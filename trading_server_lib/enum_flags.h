#ifndef _H_2E06FD07_3ABC_49CA_A172_4260C8D7AF3C_
#define _H_2E06FD07_3ABC_49CA_A172_4260C8D7AF3C_

#include <initializer_list>
#include <type_traits>

namespace utils {

	template <typename E>
	class CEnumFlags
	{
		using under_t = typename std::underlying_type<E>::type;
#define	scu(value) static_cast<under_t>(value)
	public:
		CEnumFlags(E value)
			: m_value(scu(value))
		{}
		
		CEnumFlags(std::initializer_list<E> values)
		{
			for (const auto& v : values) {
				Set(v);
			}
		}


		void Set(E value) {
			m_value |= scu(value);
		}

		bool IsSet(E value) {
			return 0 != (m_value & scu(value));
		}
	private:
		under_t m_value{ 0 };
	};

}

#endif // _H_2E06FD07_3ABC_49CA_A172_4260C8D7AF3C_
