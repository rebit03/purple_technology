#ifndef _H_4EBAEA00_8F4C_4B1B_8470_7BC4DE4D5FDA_
#define _H_4EBAEA00_8F4C_4B1B_8470_7BC4DE4D5FDA_

#include "enum_flags.h"
#include "types.h"

#include <optional>

#include <stdint.h>

namespace tradingServer {

	/**
	  * \brief Trading server communication interface
	  */
	class ITradingServer
	{
	public:
		virtual ~ITradingServer() {}

		/**
		  * \brief Connect to a trading server
		  * \param[in] remote IPv4 address of the trading server
		  */
		virtual void Connect(const char* remote) = 0;
		/**
		  * \brief Disconnect from the trading server
		  */
		virtual void Disconnect() = 0;
		/**
		  * \brief Check if a connection to the trading server is established
		  * \return true if the connection is established, false otherwise
		  */
		virtual bool IsConnected() = 0;
		/**
		  * \brief Login to the trading server
		  * \param[in] login User id
		  * \param[in] pwd User password
		  */
		virtual void Login(int32_t login, const char* pwd) = 0;

		/**
		  * \brief Get groups available on the trading server
		  * \return Collection of available groups
		  */
		virtual groups_container GetGroups() = 0;
		/**
		  * \brief Get information about a group on the trading server
		  * \param[in] name Name of the group to get information about
		  * \return Group info if the group is available, nullopt otherwise
		  */
		virtual std::optional<SGroupInfo> GetGroupInfo(const char* name) = 0;
		/**
		  * \brief Get information about an user on the trading server
		  * \param[in] login ID of the user to get information about
		  * \return User info if the user is available, nullopt otherwise
		  */
		virtual std::optional<SUserInfo> GetUserInfo(int32_t login) = 0;
		/**
		  * \brief Get number of users on the trading server
		  * \return Number of users on the trading server
		  */
		virtual int32_t GetUsersCount() = 0;
		/**
		  * \brief Get number of users on the trading server in specified groups
		  * \param[in] groups Container of groups to which the users should be associated to
		  * \return Number of users on the trading server in specified groups
		  */
		virtual int32_t GetUsersCount(const groups_container& groups) = 0;
		/**
		  * \brief Get information about trades in specified groups
		  * \param[in] groups Container of groups to which the trades should be associated to
		  * \param[in] commands Get only trades with specified commands
		  * \return Container of trades info in specified groups and with specified commands
		  */
		virtual trades_container GetTrades(const groups_container& groups, utils::CEnumFlags<ETradeCommand> commands = ETradeCommand::all) = 0;

		/**
		  * \brief Switch server to real-time notification mode
		  * \param[in] fn Notification callback function
		  * \param[in] notifications Receive only notifications of specified type
		  */
		virtual void SwitchToNotificationMode(const notify_callback& callbackFn, utils::CEnumFlags<ENotificationType> notifications = ENotificationType::all) = 0;
	};

}

#endif // _H_4EBAEA00_8F4C_4B1B_8470_7BC4DE4D5FDA_
