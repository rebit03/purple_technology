#include "stdafx.h"
#include "metaquotes.h"

#include <algorithm>
#include <experimental/filesystem>
#include <future>

namespace tradingServer {

	using namespace std;

	namespace {

		const char* g_metaquotesLibPath = "\\3rd_party\\metaquotes\\bin\\mtmanapi64.dll";	//!< path to the 3rd party Metaquotes MT4 trading server communication library

		/**
		  * \brief Convert a groups container to a comma separated string of group names
		  * \param[in] groups Groups container intendet to stringify
		  * \return Comma separated string of group names
		  */
		string stringifyGroups(const groups_container& groups)
		{
			string groupsStr;
			for (const auto& group : groups) {
				if (!groupsStr.empty()) {
					groupsStr.push_back(',');
				}
				groupsStr.append(group.name);
			}
			return groupsStr;
		}

		/**
		  * \brief Metaquotes MT4 trading server notification callback
		  * \param[in] code Type of notification data
		  * \param[in] type Type of notification data operation (add, update, delete)
		  * \param[in] pData Pointer to notification data
		  * \param[in] pParam Pointer to CMetaquotesTradingServer instance
		  */
		void onNotificationReceived(int32_t code, int32_t type, void* pData, void* pParam)
		{
			static_cast<CMetaquotesTradingServer*>(pParam)->OnNotificationReceived(code, type, pData);
		}

		/**
		  * \brief Checks if string starts with \"demo\"
		  * \param[in] str String to check
		  * \return true if the string starts with \"demo\", false otherwise
		  */
		bool isDemo(const char* str)
		{
			static const string demoStr("demo");
			return (0 == ::strncmp(str, demoStr.c_str(), std::min<size_t>(strlen(str), demoStr.size())));
		}

		/**
		  * \brief Convert Metaquotes MT4 trade command to equivalent ETradeCommand
		  * \param[in] cmd Metaquotes MT4 trade command
		  * \return equivalent ETradeCommand
		  */
		ETradeCommand cmdToTradeCommand(int32_t cmd)
		{
			switch (cmd) {
				case OP_BUY: return ETradeCommand::buy;
				case OP_SELL: return ETradeCommand::sell;
				case OP_BUY_LIMIT: return ETradeCommand::buy_limit;
				case OP_SELL_LIMIT: return ETradeCommand::sell_limit;
				case OP_BUY_STOP: return ETradeCommand::buy_stop;
				case OP_SELL_STOP: return ETradeCommand::sell_stop;
				case OP_BALANCE: return ETradeCommand::balance;
				case OP_CREDIT: return ETradeCommand::credit;
				default: return ETradeCommand::unknown;
			}
		}

		/**
		  * \brief Convert Metaquotes MT4 notification operation to equivalent ENotificationOp
		  * \param[in] op Metaquotes MT4 notification operation
		  * \return equivalent ENotificationOp
		  */
		ENotificationOp notificationTypeToOp(int32_t op)
		{
			switch (op) {
				case TRANS_ADD: return ENotificationOp::add;
				case TRANS_UPDATE: return ENotificationOp::update;
				case TRANS_DELETE: return ENotificationOp::remove;
				default: return ENotificationOp::unknown;
			}
		}

		/**
		  * \brief Convert Metaquotes MT4 user recor to SUserInfo structure
		  * \param[in] record Metaquotes MT4 user record
		  * \return SUserInfo structure
		  */
		SUserInfo getUserInfo(const UserRecord& record)
		{
			return SUserInfo{
				record.login,
				record.balance,
				record.group
			};
		}

		/**
		  * \brief Convert Metaquotes MT4 group info to SGroupInfo structure
		  * \param[in] info Metaquotes MT4 group info
		  * \return SGroupInfo structure
		  */
		SGroupInfo getGroupInfo(const ConGroup& info)
		{
			return SGroupInfo{
				info.group,
				info.currency
			};
		}

		/**
		 * \brief Convert Metaquotes MT4 trade record info to STradeInfo structure
		 * \param[in] record Metaquotes MT4 trade record info
		 * \return STradeInfo structure
		 */
		STradeInfo getTradeInfo(const TradeRecord& record)
		{
			return STradeInfo{
				record.order,
				cmdToTradeCommand(record.cmd),
				record.login, // says owner in TradeRecord struct - is it the user?
				record.volume,
				record.open_time,
				record.close_time,
				record.profit
			};
		}

		/**
		  * \brief Convert Metaquotes MT4 trade request info to STradeRequestInfo structure
		  * \param[in] info Metaquotes MT4 trade request info
		  * \return STradeRequestInfo structure
		  */
		STradeRequestInfo getTradeRequestInfo(const RequestInfo& info)
		{
			return STradeRequestInfo{
				info.id,
				cmdToTradeCommand(info.trade.cmd),
				info.time,
				info.trade.volume,
				info.login,
				info.balance,
				info.group
			};
		}
	}

	CMetaquotesTradingServer::CMetaquotesTradingServer()
		: m_metaquotesMgrFactory(experimental::filesystem::current_path().append(g_metaquotesLibPath).string().c_str())
		, m_pMetaquotesMgr(m_metaquotesMgrFactory.Create(ManAPIVersion))
	{
		if (NULL == m_pMetaquotesMgr) {
			// TODO: throw exception
		}
		if (RET_OK != m_metaquotesMgrFactory.WinsockStartup()) {
			m_pMetaquotesMgr->Release();
			m_pMetaquotesMgr = NULL;
			// TODO: throw exception
		}
		m_resourceDeleter = bind(&CManagerInterface::MemFree, m_pMetaquotesMgr, placeholders::_1);	// bind interface function as a deleter
	}

	CMetaquotesTradingServer::~CMetaquotesTradingServer()
	{
		if (m_pMetaquotesMgr->IsConnected()) {
			m_pMetaquotesMgr->Disconnect();
		}
		m_pMetaquotesMgr->Release();
		m_pMetaquotesMgr = NULL;

		m_metaquotesMgrFactory.WinsockCleanup();
	}

	void CMetaquotesTradingServer::Connect(const char* remote)
	{
		const auto result{ m_pMetaquotesMgr->Connect(remote) };
		if (RET_OK != result) {
			// TODO: return error code
		}
	}

	void CMetaquotesTradingServer::Disconnect()
	{
		if (m_pMetaquotesMgr->IsConnected()) {
			m_pMetaquotesMgr->Disconnect();
		}
	}

	bool CMetaquotesTradingServer::IsConnected()
	{
		return m_pMetaquotesMgr->IsConnected();
	}

	void CMetaquotesTradingServer::Login(int32_t login, const char* pwd)
	{
		const auto result{ m_pMetaquotesMgr->Login(login, pwd) };
		if (RET_OK != result) {
			// TODO: return error code
		}
	}

	groups_container CMetaquotesTradingServer::GetGroups()
	{
		int32_t groupsCount{ 0 };
		GroupUniquePtr ptr{ m_pMetaquotesMgr->GroupsRequest(&groupsCount), m_resourceDeleter };

		groups_container groups;
		{	// get just demo* groups
			auto const pGroups{ ptr.get() };
			for (int32_t i{ 0 }; i < groupsCount; ++i) {
				const auto& group{ pGroups[i] };
				if (isDemo(group.group)) {
					groups.insert(getGroupInfo(group));
				}
			}
		}

		return groups;
	}

	std::optional<SGroupInfo> CMetaquotesTradingServer::GetGroupInfo(const char* name)
	{
		ConGroup* pGroup{ nullptr };
		if (S_OK != m_pMetaquotesMgr->GroupRecordGet(name, pGroup)) {
			// TODO: exception or return nullopt
		}

		if (pGroup) {
			GroupUniquePtr ptr{ pGroup };
			return getGroupInfo(*ptr);
		}
		else {
			return nullopt;
		}
	}

	std::optional<SUserInfo> CMetaquotesTradingServer::GetUserInfo(int32_t login)
	{
		int32_t usersCount{ 0 };
		UserRecord* pUser{ m_pMetaquotesMgr->UserRecordsRequest(&login, &usersCount) };

		if (pUser) {
			UserRecordUniquePtr ptr{ pUser };
			return getUserInfo(*ptr);
		}
		else {
			return nullopt;
		}
	}

	int32_t CMetaquotesTradingServer::GetUsersCount()
	{
		int32_t usersCount{ 0 };
		UserRecordUniquePtr ptr{ m_pMetaquotesMgr->UsersRequest(&usersCount), m_resourceDeleter };

		int32_t demoUsersCount{ 0 };
		{	// get just users in demo* groups
			auto const users{ ptr.get() };
			for (int32_t i{ 0 }; i < usersCount; ++i) {
				const auto& user{ users[i] };
				if (isDemo(user.group)) {
					demoUsersCount++;
				}
			}
		}

		return demoUsersCount;
	}

	int32_t CMetaquotesTradingServer::GetUsersCount(const groups_container& groups)
	{
		if (groups.empty()) {
			return 0;
		}

		int32_t usersCount{ 0 };
		UserRecordUniquePtr ptr{ m_pMetaquotesMgr->AdmUsersRequest(stringifyGroups(groups).c_str(), &usersCount), m_resourceDeleter };

		return usersCount;
	}

	trades_container CMetaquotesTradingServer::GetTrades(const groups_container& groups, utils::CEnumFlags<ETradeCommand> commands/* = ETradeCommand::all*/)
	{
		if (groups.empty()) {
			return trades_container();
		}

		int32_t tradesCount{ 0 };
		TradeRecordUniquePtr ptr{ m_pMetaquotesMgr->AdmTradesRequest(stringifyGroups(groups).c_str(), FALSE, &tradesCount), m_resourceDeleter };

		trades_container trades;
		auto const pTrades{ ptr.get() };
		for (int32_t i{ 0 }; i < tradesCount; ++i) {
			const auto& trade{ pTrades[i] };
			if (commands.IsSet(cmdToTradeCommand(trade.cmd))) {
				trades.push_back(getTradeInfo(trade));
			}
		}

		return trades;
	}

	void CMetaquotesTradingServer::SwitchToNotificationMode(const notify_callback& callbackFn, utils::CEnumFlags<ENotificationType> notifications/* = ENotificationTypes::all*/)
	{
		const auto result{ m_pMetaquotesMgr->PumpingSwitchEx(
			onNotificationReceived,
			CLIENT_FLAGS_HIDETICKS | CLIENT_FLAGS_HIDENEWS | CLIENT_FLAGS_HIDEMAIL | CLIENT_FLAGS_HIDEONLINE | CLIENT_FLAGS_HIDEUSERS,
			this
		) };
		if (RET_OK != result) {
			// TODO: return error code
		}
		m_notifications = notifications;
		m_notificationCallback = callbackFn;
	}

	void CMetaquotesTradingServer::OnNotificationReceived(int32_t code, int32_t type, void* pData)
	{
		// TODO: no need for MemFree on pData?
		if (!pData) {
			return;
		}

		switch (code) {
			case PUMP_UPDATE_USERS: {
				//if (m_notifications.IsSet(ENotificationTypes::users)) {
				//	const auto ptr{ static_cast<UserRecord*>(pData) };
				//}
			} break;
			case PUMP_UPDATE_TRADES: {
				if (m_notifications.IsSet(ENotificationType::trade)) {
					auto pTrade{ make_unique<CNotificationTradeInfo>(notificationTypeToOp(type)) };

					pTrade->tradeInfo = getTradeInfo(*static_cast<TradeRecord*>(pData));

					m_notificationCallback(move(pTrade));
				}
			} break;
			case PUMP_UPDATE_GROUPS: {
				//if (m_notifications.IsSet(ENotificationTypes::groups)) {
				//	const auto ptr{ static_cast<ConGroup*>(pData) };
				//}
			} break;
			case PUMP_UPDATE_REQUESTS: {
				if (m_notifications.IsSet(ENotificationType::trade_request)) {
					auto pRequests{ make_unique<CNotificationTradeRequestInfo>(notificationTypeToOp(type)) };

					pRequests->requestInfo = getTradeRequestInfo(*static_cast<RequestInfo*>(pData));

					m_notificationCallback(move(pRequests));
				}
			} break;
			default: {
				// nothing
			} break;
		}
	}
}
