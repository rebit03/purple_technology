#ifndef _H_06FBEC96_566C_4CA5_9B66_9A44DB1C9448_
#define _H_06FBEC96_566C_4CA5_9B66_9A44DB1C9448_

#include "types.h"
#include "trading_server_intf.h"

#include <memory>

namespace tradingServer {

	/**
	  * \brief Factory functino to create specified trading server implementation
	  * \param[in] type Type of trading server implementation
	  * \return Pointer to trading server implementation
	  */
	std::unique_ptr<ITradingServer> getTradingServer(ETradingServerType type);

}

#endif // _H_06FBEC96_566C_4CA5_9B66_9A44DB1C9448_
