#ifndef _H_11279BBD_3B0B_4861_BCE4_B350898F566F_
#define _H_11279BBD_3B0B_4861_BCE4_B350898F566F_

#include "trading_server_intf.h"

#include <functional>
#include <memory>

#include <Windows.h>
#include <Winsock2.h>
#include <3rd_party/metaquotes/MT4ManagerAPI.h>

namespace tradingServer {

	/**
	  * \brief Metaquotes MT4 trading server communication implementation
	  */
	class CMetaquotesTradingServer : public ITradingServer
	{
	public:
		CMetaquotesTradingServer();
		~CMetaquotesTradingServer();

		// ITradingServer
		void Connect(const char* remote) override;
		void Disconnect() override;
		bool IsConnected() override;
		void Login(int32_t login, const char* pwd) override;

		groups_container GetGroups() override;
		std::optional<SGroupInfo> GetGroupInfo(const char* name) override;
		std::optional<SUserInfo> GetUserInfo(int32_t login) override;
		int32_t GetUsersCount() override;
		int32_t GetUsersCount(const groups_container& groups) override;
		trades_container GetTrades(const groups_container& groups, utils::CEnumFlags<ETradeCommand> commands = ETradeCommand::all) override;

		void SwitchToNotificationMode(const notify_callback& callbackFn, utils::CEnumFlags<ENotificationType> notifications = ENotificationType::all) override;
	public:
		/**
		  * \brief Handle new notification from the trading server
		  * \param[in] code Type of notification data
		  * \param[in] type Type of notification data operation (add, update, delete)
		  * \param[in] pData Pointer to notification data
		  */
		void OnNotificationReceived(int32_t code, int32_t type, void* pData);
	private:
		CManagerFactory							m_metaquotesMgrFactory;	//!< Metaquotes MT4 trading server communication dll factory
		CManagerInterface*						m_pMetaquotesMgr;		//!< Metaquotes MT4 trading server communication interface
		using MetaquotesDeleter = std::function<void(void*)>;			//!< Metaquotes MT4 dynamic resources deleter type
		MetaquotesDeleter						m_resourceDeleter;		//!< Metaquotes MT4 dynamic resources deleter
		utils::CEnumFlags<ENotificationType>	m_notifications{ ENotificationType::none };	//!< Notifications filter
		notify_callback							m_notificationCallback;	//!< Notifications callback function
	private:
		using GroupUniquePtr = std::unique_ptr<ConGroup, MetaquotesDeleter>;			//!< Metaquotes MT4 group info pointer
		using UserRecordUniquePtr = std::unique_ptr<UserRecord, MetaquotesDeleter>;		//!< Metaquotes MT4 user info pointer
		using TradeRecordUniquePtr = std::unique_ptr<TradeRecord, MetaquotesDeleter>;	//!< Metaquotes MT4 trade info pointer
	};

}

#endif // _H_11279BBD_3B0B_4861_BCE4_B350898F566F_
