#include "stdafx.h"
#include "trading_server.h"
#include "metaquotes.h"

namespace tradingServer {

	using namespace std;

	unique_ptr<ITradingServer> getTradingServer(ETradingServerType type)
	{
		switch (type) {
			default: // TODO: default should throw exception, all valid values should return some implementation
			case ETradingServerType::metaquotes: {
				return move(make_unique<CMetaquotesTradingServer>());
			} break;
		}
	}

}
