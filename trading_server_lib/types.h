#ifndef _H_BB31DB56_1FD1_49E1_93E8_0898F6F7E795_
#define _H_BB31DB56_1FD1_49E1_93E8_0898F6F7E795_

#include <functional>
#include <memory>
#include <string>
#include <unordered_set>
#include <vector>

#include <stdint.h>

namespace tradingServer {

	///////////////////////////////////////////////////////////////////////////////////////////////
	// TRADING SERVER

	/**
	  * \brief Type of trading server
	  */
	enum class ETradingServerType
	{
		metaquotes //!< Metaquotes MT4 trading server
	};

	/**
	  * \brief Structure representing information for connection and login to trading server
	  */
	struct SRemote {
		tradingServer::ETradingServerType serverType; //!< type of trading server
		int32_t		login{ -1 };	//!< user login id
		std::string	pwd;			//!< user login password
		std::string	ip;				//!< server IPV4 address
	};

	/**
	  * \brief Container for multiple remote structures
	  */
	using remotes_container = std::vector<SRemote>;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// USERS

	/**
	  * \brief Structure representing information about an user on a trading server
	  */
	struct SUserInfo
	{
		int32_t		login{ -1 };	//!< user login id
		double		balance{ 0 };	//!< user balance
		std::string	group;			//!< name of a group the user is associated to

		bool operator==(const SUserInfo& rhs) const
		{
			return rhs.login == login;
		}
	};

	/**
	  * \brief Helper structure for hash calculation
	  */
	struct SUserInfoHash
	{
		size_t operator()(const SUserInfo& info) const
		{
			std::hash<int32_t> hash;
			return hash(info.login);
		}
	};

	/**
	  * \brief Container for multiple user info structures
	  */
	using users_container = std::unordered_set<SUserInfo>;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// GROUPS

	/**
	  * \brief Structure representing information about a group on a trading server
	  */
	struct SGroupInfo
	{
		std::string name;		//!< group name
		std::string currency;	//!< group currency name

		bool operator==(const SGroupInfo& rhs) const
		{
			return (0 == name.compare(rhs.name)) && (0 == currency.compare(rhs.currency));
		}
	};

	/**
	  * \brief Helper structure for hash calculation
	  */
	struct SGroupInfoHash
	{
		size_t operator()(const SGroupInfo& info) const
		{
			std::hash<std::string> hash;
			return hash(info.name);
		}
	};

	/**
	  * \brief Container for multiple group info structures
	  */
	using groups_container = std::unordered_set<SGroupInfo, SGroupInfoHash>;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// TRADES

	/**
	  * \brief Definition of possible trade commands, may be used as flags
	  */
	enum class ETradeCommand : int32_t
	{
		unknown		= 0x00,
		buy			= 0x01,
		sell		= 0x02,
		buy_limit	= 0x04,
		sell_limit	= 0x08,
		buy_stop	= 0x10,
		sell_stop	= 0x20,
		balance		= 0x40,
		credit		= 0x80,
		all			= 0x7FFFFFFF
	};

	/**
	  * \brief Structure representing information about a trade on a trading server
	  */
	struct STradeInfo
	{
		int32_t			id{ -1 };							//!< trade id
		ETradeCommand	command{ ETradeCommand::unknown };	//!< trade command
		int32_t			userLogin{ -1 };					//!< user login which is an owner of the trade
		int32_t			volume{ 0 };						//!< trade volume
		__time32_t		openTime{ 0 };						//!< open time of a trade (seconds since EPOCH)
		__time32_t		closeTime{ 0 };						//!< close time of a trade (seconds since EPOCH)
		double			profit{ 0 };						//!< trade profit

		/**
		  * \brief Checks if the trade is closed
		  * \return true if the trade is closed, false otherwise
		  */
		bool IsClosed() const { return 0 != closeTime; }
	};

	/**
	  * \brief Container for multiple trade info structures
	  */
	using trades_container = std::vector<STradeInfo>;

	///////////////////////////////////////////////////////////////////////////////////////////////
	// TRADE REQUESTS

	/**
	  * \brief Structure representing information about a trade request on a trading server
	  */
	struct STradeRequestInfo
	{
		int32_t			id;									//!< trade request id
		ETradeCommand	command{ ETradeCommand::unknown };	//!< trade command
		uint32_t		time{ 0 };							//!< trade request creation time
		int32_t			volume{ 0 };						//!< trade volume
		int32_t			userLogin{ -1 };					//!< user id
		double			userBalance{ 0 };					//!< user balance
		std::string		groupName;							//!< group to which the user is associated to
	};

	///////////////////////////////////////////////////////////////////////////////////////////////
	// NOTIFICATIONS

	/**
	  * \brief Definition of possible notification types, may be used as flags
	  */
	enum class ENotificationType : int32_t
	{
		none			= 0x00,
		user			= 0x01,	//!< notifications about users
		trade			= 0x02,	//!< notifications about trades
		group			= 0x04,	//!< notifications about groups
		trade_request	= 0x08,	//!< notifications about trade requests
		all				= 0x7FFFFFFF
	};

	/**
	  * \brief Definition of possible notification operations
	  */
	enum class ENotificationOp
	{
		unknown,
		add,	//!< notification object was created
		update,	//!< notification object was updated
		remove	//!< notification object was deleted
	};

	/**
	  * \brief Notification data interface
	  */
	class INotificationData
	{
	public:
		INotificationData(ENotificationType type, ENotificationOp op)
			: type(type)
			, op(op)
		{}
		virtual ~INotificationData() {}

		const ENotificationType type;	//!< data type
		const ENotificationOp	op;		//!< data operation
	};

	/**
	  * \brief Pointer to notification data type
	  */
	using notification_data_ptr = std::unique_ptr<INotificationData>;

	/**
	  * \brief Notification callback function type
	  */
	using notify_callback = std::function<void(notification_data_ptr)>;

	/**
	  * \brief Trade request notification data
	  */
	class CNotificationTradeRequestInfo : public INotificationData
	{
	public:
		explicit CNotificationTradeRequestInfo(ENotificationOp op)
			: INotificationData(ENotificationType::trade_request, op)
		{}

		STradeRequestInfo requestInfo;	//!< trade request info
	};

	/**
	  * \brief Trade info notification data
	  */
	class CNotificationTradeInfo : public INotificationData
	{
	public:
		explicit CNotificationTradeInfo(ENotificationOp op)
			: INotificationData(ENotificationType::trade, op)
		{}

		STradeInfo tradeInfo;	//!< trade info
	};

}

#endif // _H_BB31DB56_1FD1_49E1_93E8_0898F6F7E795_
