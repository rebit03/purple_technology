#include "stdafx.h"
#include "statistics.h"

#include <experimental/filesystem>

int main(int argc, char* argv[])
{
	// TODO: loading config from config.json
	// TODO: mapping from string to server type
	/*
	config.json

	remotes: [
		{
			server_type: "metaqotes",
			login : <num>|"login",
			pwd : "pwd",
			ipv4 : "ip",
			[port : <num>]
		},
		...
	],
	output_file: "file_name"
	*/
	const auto trades{ dumper::statistics::getTradesStatistics({
		{ tradingServer::ETradingServerType::metaquotes, 60, "Hqy4hbR", "134.213.9.138" },
		{ tradingServer::ETradingServerType::metaquotes, 61, "ilbial", "134.213.9.138" },
		{ tradingServer::ETradingServerType::metaquotes, 62, "afi3kxx", "92.52.99.147" }
	}) };

	writeToCSV(trades, std::experimental::filesystem::current_path().append("\\trades_stats.csv").string().c_str());

	// TODO: exceptions

	return 0;
}
