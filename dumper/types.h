#ifndef _H_BA6C1C2E_D511_4A65_B770_AFD8172E5C01_
#define _H_BA6C1C2E_D511_4A65_B770_AFD8172E5C01_

#include<trading_server_lib/types.h>

#include <string>
#include <vector>

#include <stdint.h>

namespace dumper {
	namespace statistics {

		/**
		  * \brief Structure representing information about trades statistics
		  */
		struct STradesStats
		{
			int32_t	count{ 0 };		//!< number of trades
			int32_t	volume{ 0 };	//!< trades volume
			double	profit{ 0 };	//!< trades profit
		};

	}
}

#endif // _H_BA6C1C2E_D511_4A65_B770_AFD8172E5C01_
