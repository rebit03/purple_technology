#include "stdafx.h"
#include "statistics.h"

#include <trading_server_lib/trading_server.h>

#include <condition_variable>
#include <future>
#include <iomanip>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>
#include <type_traits>

#include <Windows.h>

namespace dumper {
	namespace statistics {

		using namespace std;
		using namespace tradingServer;

		namespace {

			/**
			  * \brief Synchronization barrier for multiple threads
			  */
			class CBarrier
			{
			public:
				explicit CBarrier(size_t arrivals)
					: m_arrivals(arrivals)
				{}

				/**
				  * \brief Wait for other threads to reach the barrier
				  */
				void Wait()
				{
					unique_lock<mutex> lock(m_mutex);

					m_arrivals--;

					if (0 == m_arrivals) {
						m_cv.notify_all();
					}
					else {
						m_cv.wait(lock, [&] { return 0 == m_arrivals; });
					}
				}
			private:
				mutex				m_mutex;	//!< synchronization mutex for number of threads
				size_t				m_arrivals;	//!< number of threads to wait for
				condition_variable	m_cv;		//!< notification for waiting threads
			};

			/**
			  * \brief Threads shared source holding max. number of users and its corresponding thread id
			  */
			class CMaxUsersThread
			{
			public:
				CMaxUsersThread() = default;

				/**
				  * \brief Set number of users and thread id if current user count is lower
				  * \param[in] userCount Number of users to set
				  * \param[in] threadId Thread id
				  */
				void Update(int32_t userCount, thread::id threadId)
				{
					lock_guard<mutex> lock(m_mutex);
					if (userCount > m_userCount) {
						m_userCount = userCount;
						m_threadId = threadId;
					}
				}

				const thread::id& GetId() const
				{
					return m_threadId;
				}
			private:
				mutex		m_mutex;			//!< synchronization mutex
				int32_t		m_userCount{ 0 };	//!< max. user count received from threads
				thread::id	m_threadId;			//!< thread id corresponding to the user count
			};

			void getTradeStatistics(STradesStats& tradesStats, const SRemote& remote, CMaxUsersThread& usersThread, CBarrier& barrier)
			{
				const auto pServer{ getTradingServer(remote.serverType) };

				groups_container groups;

				const auto threadId{ this_thread::get_id() };

				pServer->Connect(remote.ip.c_str());
				if (pServer->IsConnected()) {
					pServer->Login(remote.login, remote.pwd.c_str());
					groups = pServer->GetGroups();
					usersThread.Update(pServer->GetUsersCount(groups), threadId);
				}

				barrier.Wait(); // wait for other threads to get their user count

				if (threadId == usersThread.GetId())
				{	// thread with the most users
					const auto trades{ pServer->GetTrades(groups,{ ETradeCommand::buy, ETradeCommand::sell }) };

					{	// stats
						int32_t count{ 0 };
						int64_t volume{ 0 };
						long double profit{ 0 };
						for (const auto& trade : trades) {
							if (trade.IsClosed()) {
								count++;
								profit += trade.profit;
								volume += trade.volume;
							}
						}

						if (0 < count) {
							tradesStats.count = count;
							tradesStats.volume = static_cast<int32_t>(volume / count);
							tradesStats.profit = static_cast<double>(profit / count);
						}
					}
				}
			}
		
		}

		STradesStats getTradesStatistics(const remotes_container& remotes)
		{
			STradesStats stats;

			CMaxUsersThread usersThread;
			CBarrier barrier(remotes.size());
			vector<future<void>> threads;
			for (const auto& remote : remotes) {
				threads.push_back(async(launch::async, getTradeStatistics, ref(stats), ref(remote), ref(usersThread), ref(barrier)));
			}

			for (const auto& thr : threads) {
				thr.wait();
			}

			return stats;
		}

		void writeToCSV(const STradesStats& stats, const char* filePath)
		{
			HANDLE fileHandle = ::CreateFile(
				filePath,
				GENERIC_WRITE,
				0,
				NULL,
				CREATE_ALWAYS,
				FILE_ATTRIBUTE_NORMAL,
				NULL
			);

			if (INVALID_HANDLE_VALUE == fileHandle) {
				// TODO: throw exception
				return;
			}

			string fileData;
			try {
				ostringstream ostr("total_count,average_volume,average_profit\n", ios_base::ate);	// file header
				ostr << stats.count << "," << stats.volume << "," << showpos << fixed << setprecision(9) << stats.profit << endl;

				fileData.assign(ostr.str());
			}
			catch (...) {
				::CloseHandle(fileHandle);
				throw;
			}

			// TODO: handle result?
			::WriteFile(fileHandle, fileData.c_str(), static_cast<DWORD>(fileData.size()), NULL, NULL);

			::CloseHandle(fileHandle);
		}

	}
}
