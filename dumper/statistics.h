#ifndef _H_66723B07_57F2_4BD4_9AD0_3EABAF869919_
#define _H_66723B07_57F2_4BD4_9AD0_3EABAF869919_

#include "types.h"

#include <trading_server_lib/types.h>

namespace dumper {
	namespace statistics {

		/**
		  * \brief Get trades statistics from trading server with the most users
		  * \param[in] remotes Container of remote trading servers
		  * \return Trading server trades statistics
		  */
		STradesStats getTradesStatistics(const tradingServer::remotes_container& remotes);
		/**
		  * \brief Write statistics to CSV file
		  * \param[in] stats Trades statistics
		  * \param[in] filePath Path to CSV output file
		  */
		// TODO: if it's neccessary to write more class types, create serializable interface
		void writeToCSV(const STradesStats& stats, const char* filePath);

	}
}

#endif // _H_66723B07_57F2_4BD4_9AD0_3EABAF869919_
