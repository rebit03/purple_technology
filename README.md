# Trading utils

> C++ programming exercise ([Purple Technology](https://www.cocuma.cz/job/cc-developer/))  
> See **assignment.pdf** in the root for details (CZ)

## Project structure

### dumper
Command line utility - outputs trading statistics to CSV file (trades_stats.csv)

### trading_server_lib
Implementation of communication with trading servers

### watcher
Command line utility - watches for suspicious trades on trading servers  
There are two core classes - notifier and comparator.  
#### CTradesNotifier
Notifier connects to two instances of one remote trading server (so there is one
notifier object for each server).
One intance receives real-time notifications and the other retrieves further information
about incoming notifications. Notifications are stored in a queue with exclusive access
because of connections to the server. Notifications are packed with further information
and forwarded to comparator.
#### CTradesComparator
Trades comparator is shared across all notifier objects and stores incoming trades
ordered by their open time. The container of trades has exclusive acess so new trades
can be compared with others.

## TODOs
- [ ] TODOs in code
- [ ] exceptions/errors handling
- [ ] loading of configuration from json file
- [ ] some tests would be nice
