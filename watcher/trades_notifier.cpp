#include "stdafx.h"
#include "trades_notifier.h"
#include "types.h"

#include <algorithm>
#include <string>
#include <type_traits>

namespace watcher
{

	using namespace std;
	using namespace tradingServer;

	namespace {

		/**
		  * \brief Checks if string starts with \"demo\"
		  * \param[in] str String to check
		  * \return true if the string starts with \"demo\", false otherwise
		  */
		bool isDemo(const char* str)
		{
			static const string demoStr("demo");
			return (0 == ::strncmp(str, demoStr.c_str(), std::min<size_t>(strlen(str), demoStr.size())));
		}

	}

	CTradesNotifier::CTradesNotifier(const SRemote& remote, const shared_ptr<IComparator>& pCmp)
		: m_pComparator(pCmp)
		, m_remote(remote)
		, m_pInfoServer(getTradingServer(remote.serverType))
		, m_pPumpServer(getTradingServer(remote.serverType))
	{
		m_pumpingFuture = async(launch::async, &CTradesNotifier::StartPumping, this); // connect and login to server async so creation of the object is nonblocking
	}

	CTradesNotifier::~CTradesNotifier()
	{
		CleanUp();
	}

	void CTradesNotifier::CleanUp()
	{
		m_stopProcessing = true;
		if (m_pumpingFuture.valid()) {
			m_pumpingFuture.wait();	// wait for connection and login
		}
		if (m_processingFuture.valid()) {
			m_tradesQueueAlarm.notify_one(); // notify processing thread - may be waiting for new data
			m_processingFuture.wait(); // wait for processing thread to finish
		}
		// TODO: handle Enqueue futures, can crash when not waiting in destructor
	}

	void CTradesNotifier::StartPumping()
	{
		m_pInfoServer->Connect(m_remote.ip.c_str());
		m_pPumpServer->Connect(m_remote.ip.c_str());
		if (!m_pInfoServer->IsConnected() || !m_pPumpServer->IsConnected()) {
			// TODO: exception
			return;
		}
		m_pInfoServer->Login(m_remote.login, m_remote.pwd.c_str());
		m_pPumpServer->Login(m_remote.login, m_remote.pwd.c_str());

		m_processingFuture = async(launch::async, &CTradesNotifier::ProcessTradesQueue, this); // prepare processing thread

		notify_callback notifyFunction{ bind(&CTradesNotifier::OnNewTradeData, this, placeholders::_1) };
		// TODO: don't know if to process new trades or if it's better to check new trade requests
		m_pPumpServer->SwitchToNotificationMode(notifyFunction, ENotificationType::trade);
	}

	void CTradesNotifier::OnNewTradeData(notification_data_ptr pData)
	{
		async(launch::async, &CTradesNotifier::Enqueue, this, move(pData)); // handle new data async so the callback is not blocking notification thread
	}

	void CTradesNotifier::Enqueue(notification_data_ptr pData)
	{
		if ((ENotificationType::trade != pData->type) || (ENotificationOp::add != pData->op))
		{	// handle only new trades
			return;
		}

		auto pTradeInfo{ static_cast<CNotificationTradeInfo*>(pData.get()) };

		if (nullptr == pTradeInfo) {
			return;
		}

		auto& tradeInfo{ pTradeInfo->tradeInfo };

		if ((ETradeCommand::buy != tradeInfo.command) && (ETradeCommand::sell != tradeInfo.command))
		{	// handle only buy or sell trades
			return;
		}

		lock_guard<mutex> lock(m_tradesQueueLock);
		if (!m_stopProcessing) {
			m_tradesQueue.push_back(move(tradeInfo)); // enqueue trade
			m_tradesQueueAlarm.notify_one(); // notify processing thread
		}
	}

	void CTradesNotifier::ProcessTradesQueue()
	{
		while (!m_stopProcessing)
		{	// there is no request to stop processing
			unique_lock<mutex> lock(m_tradesQueueLock); // lock access to the queue
			
			while (!m_tradesQueue.empty() && !m_stopProcessing)
			{
				tradingServer::STradeInfo tradeInfo{ m_tradesQueue.front() };
				m_tradesQueue.pop_front(); // process first trade in queue

				auto maybeUser{ m_pInfoServer->GetUserInfo(tradeInfo.userLogin) };
				if (!maybeUser || !isDemo(maybeUser->group.c_str()))
				{	// if the user is not found on the server or it's not demo group continue
					continue;
				}

				if (m_stopProcessing) {
					break;
				}

				auto maybeGroup{ m_pInfoServer->GetGroupInfo(maybeUser->group.c_str()) };
				if (!maybeGroup)
				{	// if the group is not found on the server continue
					continue;
				}

				if (m_stopProcessing) {
					break;
				}

				// compare the trade with others
				m_pComparator->operator()<void, watcher::STradeInfo>(watcher::STradeInfo{
					tradeInfo.id,
					tradeInfo.command,
					tradeInfo.volume,
					tradeInfo.openTime,
					tradeInfo.userLogin,
					maybeUser->balance,
					m_remote.ip,
					maybeGroup->currency
				});
			}

			if (!m_stopProcessing)
			{	// wait for new trades, don't sleep if there is something in the queue or there is request to stop processing
				m_tradesQueueAlarm.wait(lock, [&] { return !m_tradesQueue.empty() || m_stopProcessing; });
			}
		}
	}

}
