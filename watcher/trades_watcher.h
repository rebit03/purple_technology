#ifndef _H_8CB56D21_1C20_4DF5_9E87_F83942DF3458_
#define _H_8CB56D21_1C20_4DF5_9E87_F83942DF3458_

#include "trades_comparator.h"
#include "trades_notifier.h"

#include <trading_server_lib/types.h>

#include <memory>
#include <vector>

namespace watcher {

	/**
	  * \brief Implementation of suspicious trades watcher
	  */
	class CTradesWatcher
	{
	public:
		explicit CTradesWatcher(const tradingServer::remotes_container& remotes);
	private:
		std::shared_ptr<CTradesComparator>				m_pCmp;			//!< trades comparator
		std::vector<std::unique_ptr<CTradesNotifier>>	m_notifiers;	//!< new trades notifiers
	};

}

#endif // _H_8CB56D21_1C20_4DF5_9E87_F83942DF3458_
