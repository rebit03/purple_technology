#include "stdafx.h"
#include "trades_comparator.h"

#include <chrono>
#include <iostream>
#include <type_traits>

namespace watcher {

	using namespace std;
	using namespace std::chrono;

	namespace {

		const seconds trades_cleaner_threshold = 20s;	//!< determines how old trades keep for comparison

		/**
		  * \brief Log similar trades
		  * \param[in] lhs Trade info
		  * \param[in] rhs Trade info
		  */
		void logSuspects(const STradeInfo& lhs, const STradeInfo& rhs)
		{
			cout
				<< "1" << endl
				<< "server: " << lhs.serverIP.c_str() << endl
				<< "trade id: " << lhs.id << endl
				<< "user id: " << lhs.userLogin << endl
				<< "2" << endl
				<< "server: " << rhs.serverIP.c_str() << endl
				<< "trade id: " << rhs.id << endl
				<< "user id: " << rhs.userLogin << endl;
		}

		/**
		  * \brief Check if user balance:trade volume is similar
		  * \param[in] lhs Trade info
		  * \param[in] rhs Trade info
		  * \return true if user balance:trade volume is similar, false otherwise
		  */
		bool isVolumeBalanceRatioSimilar(const STradeInfo& lhs, const STradeInfo& rhs)
		{
			const auto lhsRatio(lhs.userBalance / lhs.volume * 100 ); // user balance to trade volume ratio in percents
			const auto rhsRatio(rhs.userBalance / rhs.volume * 100 );
			return abs(lhsRatio - rhsRatio) <= 5.0;	// difference is max 5%
		}

		/**
		  * \brief Check if trades are similar
		  * \param[in] lhs Trade info
		  * \param[in] rhs Trade info
		  * \return true if trades are similar, false otherwise
		  */
		bool suspects(const STradeInfo& lhs, const STradeInfo& rhs)
		{
			return
				lhs.openTime == rhs.openTime &&
				lhs.command == rhs.command &&
				0 == lhs.currency.compare(rhs.currency) &&
				isVolumeBalanceRatioSimilar(lhs, rhs);
		}

		/**
		  * \brief Finds and logs similar trades
		  * \param[in] info Trade info to find
		  * \param[in] list List of trades to compare to
		  */
		void compare(const STradeInfo& info, const list<STradeInfo>& list)
		{
			for (const auto& linfo : list) {
				if (suspects(linfo, info)) {
					logSuspects(linfo, info);
				}
			}
		}

	}

	CTradesComparator::CTradesComparator()
		: m_tradesCleanerFuture(async(launch::async, &CTradesComparator::TradesCleaner, this))
	{
	}

	CTradesComparator::~CTradesComparator()
	{
		CleanUp();
	}

	void CTradesComparator::CleanUp()
	{
		m_stopProcessing = true;
		m_tradesCleanerAlarm.notify_one();
		m_tradesCleanerFuture.wait();
		// TODO: handle Compare futures, can crash when not waiting in destructor
	}

	void CTradesComparator::Compare(STradeInfo tradeInfo)
	{
		lock_guard<mutex> lock(m_infoMapLock);	// runs async, synchronize access

		if (!m_stopProcessing)
		{
			if (m_lastCleaningTime >= (tradeInfo.openTime - 1))
			{	// log error when older trades were cleaned before all incoming notifications were processed
				// in case of these errors there is bottle neck and needs to increase treshold period or better notification handling
				cerr << "cleaned up trades when not all incoming notifications were processed, trade time: "
					<< tradeInfo.openTime << " cleaning time " << m_lastCleaningTime << endl;
			}

			auto& tradeTimeList{ m_infoMap[tradeInfo.openTime] }; // get list of trades with the same time
			compare(tradeInfo, tradeTimeList); // compare trades with the same time
			tradeTimeList.push_back(tradeInfo); // store new trade

			// Compare with lists at -+ 1s in map
			tradeInfo.openTime -= 1; // -1s
			const auto& before{ m_infoMap.find(tradeInfo.openTime) };
			if (before != m_infoMap.end()) {
				compare(tradeInfo, before->second); // compare trades 1s earlier
			}
			
			tradeInfo.openTime += 2; // +1s
			const auto& after{ m_infoMap.find(tradeInfo.openTime) };
			if (after != m_infoMap.end()) {
				compare(tradeInfo, after->second); // compare trades 1s later
			}
		}
	}

	void CTradesComparator::TradesCleaner()
	{
		while (!m_stopProcessing) {
			unique_lock<mutex> lock(m_infoMapLock);

			if (!m_infoMap.empty())
			{	// erase all trades older than threshold
				const __time32_t thresholdTime{ static_cast<__time32_t>(system_clock::to_time_t(system_clock::now()) - trades_cleaner_threshold.count()) };
				m_infoMap[thresholdTime]; // insert threshold time if not already there
				auto it{ m_infoMap.find(thresholdTime) }; // get threshold time iterator
				it++; // last item is exclusive, so move further
				m_infoMap.erase(m_infoMap.begin(), it); // erase old trades, older trades are lower numbers, so should be on the beggining of the map
				m_lastCleaningTime = thresholdTime;
			}

			m_tradesCleanerAlarm.wait_for(lock, seconds(trades_cleaner_threshold), [&] { return !m_infoMap.empty() || m_stopProcessing; });
		}
	}

}
