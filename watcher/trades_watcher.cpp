#include "stdafx.h"
#include "trades_watcher.h"

namespace watcher {

	using namespace std;
	using namespace tradingServer;

	CTradesWatcher::CTradesWatcher(const remotes_container& remotes)
		: m_pCmp(make_shared<CTradesComparator>())
	{
		for (const auto& remote : remotes) {
			m_notifiers.push_back(move(make_unique<CTradesNotifier>(remote, m_pCmp)));
		}
	}

}
