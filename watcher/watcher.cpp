#include "stdafx.h"
#include "trades_watcher.h"

#include <trading_server_lib/types.h>

#include <conio.h>

int main(int argc, char* argv[])
{
	watcher::CTradesWatcher tradesWatcher({
		{ tradingServer::ETradingServerType::metaquotes, 60, "Hqy4hbR", "134.213.9.138" },
		{ tradingServer::ETradingServerType::metaquotes, 61, "ilbial", "134.213.9.138" },
		{ tradingServer::ETradingServerType::metaquotes, 62, "afi3kxx", "92.52.99.147" }
	});

	// TODO: exceptions

	while (_getch() != 27);	// wait for ESC

	return 0;
}
