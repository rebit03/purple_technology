#ifndef _H_130E4D64_C291_4B3E_B6E4_698C4B2F5338_
#define _H_130E4D64_C291_4B3E_B6E4_698C4B2F5338_

#include "comparator_intf.h"
#include "types.h"

#include <atomic>
#include <condition_variable>
#include <future>
#include <list>
#include <map>
#include <mutex>
#include <future>

namespace watcher {

	/**
	  * \brief Implementation of trades comparator
	  */
	class CTradesComparator : public IComparator
	{
	public:
		CTradesComparator();
		CTradesComparator(const CTradesComparator&) = delete;
		~CTradesComparator() override;

		template <typename RType, typename ...Args>
		RType operator()(Args... args) {
			return IComparator::operator()<RType, Args... >(std::forward<Args>(args)...);
		}

		/**
		  * \brief Compare new trade info with appropriate trade infos stored already locally
		  * \param[in] tradeInfo New trade info
		  */
		void operator()(STradeInfo tradeInfo)
		{
			using fnT = void(CTradesComparator::*)(STradeInfo);
			std::async<fnT>(std::launch::async, &CTradesComparator::Compare, this, std::move(tradeInfo));
		}
	private:
		/**
		  * \brief Clean up resources on destruction
		  */
		void CleanUp();
		/**
		  * \brief Compare new trade info with appropriate trade infos stored already locally
		  * \param[in] tradeInfo New trade info
		  */
		void Compare(STradeInfo tradeInfo);
		/**
		  * \brief Removes all trades older than threshold
		  */
		void TradesCleaner();
	private:
		std::atomic_bool							m_stopProcessing{ false };	//!< synchronization primitive to stop processing of incomming trades
		std::mutex									m_infoMapLock;				//!< synchronizatio mutex to acces local trades data
		std::map<__time32_t, std::list<STradeInfo>>	m_infoMap;					//!< map of incomming trades
		__time32_t									m_lastCleaningTime{ 0 };	//!< saved last time of trades cleaning proces to compare with incoming notifications
		std::condition_variable						m_tradesCleanerAlarm;		//!< wake-up primitive to notify cleaner thread that the period was reached
		std::future<void>							m_tradesCleanerFuture;		//!< future for thread cleaning map of trades
	};

}

#endif // _H_130E4D64_C291_4B3E_B6E4_698C4B2F5338_
