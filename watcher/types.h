#ifndef _H_694CA2F9_84DC_4CDE_8AAF_F8873E663456_
#define _H_694CA2F9_84DC_4CDE_8AAF_F8873E663456_

#include <trading_server_lib/types.h>

#include <string>

#include <stdint.h>

namespace watcher {

	/**
	  * \brief Structure representing information about new trade on a trading server
	  */
	struct STradeInfo
	{
		int32_t							id{ -1 };			//!< trade id
		tradingServer::ETradeCommand	command{ tradingServer::ETradeCommand::unknown }; //!< trade command
		int32_t							volume{ 0 };		//!< trade volume
		__time32_t						openTime{ 0 };		//!< trade open time
		int32_t							userLogin{ -1 };	//!< trade owner id
		double							userBalance{ 0 };	//!< trade owner balance
		std::string						serverIP;			//!< trading server IPv4
		std::string						currency;			//!< trade group currency
	};

}

#endif // _H_694CA2F9_84DC_4CDE_8AAF_F8873E663456_
