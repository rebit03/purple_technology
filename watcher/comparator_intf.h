#ifndef _H_DE8AFA47_1923_4514_9F98_C02899CE2570_
#define _H_DE8AFA47_1923_4514_9F98_C02899CE2570_

namespace watcher {

	/**
	  * \brief General comparator class interface
	  */
	class IComparator
	{
	public:
		virtual ~IComparator() {}

		template <typename RType, typename ...Args>
		RType operator()(Args... args) {}
	};

}

#endif // _H_DE8AFA47_1923_4514_9F98_C02899CE2570_
