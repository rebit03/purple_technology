#ifndef _H_3FCFB8B7_AFF8_4292_BDAE_91407D48FE54_
#define _H_3FCFB8B7_AFF8_4292_BDAE_91407D48FE54_

#include "comparator_intf.h"

#include <trading_server_lib/trading_server.h>
#include <trading_server_lib/types.h>

#include <atomic>
#include <condition_variable>
#include <deque>
#include <future>
#include <memory>
#include <mutex>

namespace watcher {

	/**
	  * \brief Implementation of new trades notifier
	  */
	class CTradesNotifier
	{
	public:
		CTradesNotifier(const tradingServer::SRemote& remote, const std::shared_ptr<IComparator>& pCmp);
		~CTradesNotifier();
	private:
		/**
		  * \brief Clean up resources on destruction
		  */
		void CleanUp();
		/**
		  * \brief Start receiving notifications from the trading server
		  */
		void StartPumping();
		/**
		  * \brief Handle new trade data
		  * \param[in] pData Trade info notification data
		  */
		void OnNewTradeData(tradingServer::notification_data_ptr pData);
		/**
		  * \brief Enqueue new trade data for processing
		  * \param[in] pData Trade info notification data
		  */
		void Enqueue(tradingServer::notification_data_ptr pData);
		/**
		  * \brief Process queue of new trades
		  */
		void ProcessTradesQueue();
	private:
		std::shared_ptr<IComparator>					m_pComparator;				//!< trades comparator
		tradingServer::SRemote							m_remote;					//!< info about remote trading server
		std::atomic_bool								m_stopProcessing{ false };	//!< synchronization primitive to stop processing of incomming data
		std::mutex										m_tradesQueueLock;			//!< synchronization mutex for queue of new trades to process
		std::condition_variable							m_tradesQueueAlarm;			//!< wake-up primitive to notify about incoming trades to process
		std::deque<tradingServer::STradeInfo>			m_tradesQueue;				//!< queue of new trades to process
		std::unique_ptr<tradingServer::ITradingServer>	m_pInfoServer;				//!< trading server for additional requests
		std::unique_ptr<tradingServer::ITradingServer>	m_pPumpServer;				//!< trading server sending notifications
		std::future<void>								m_pumpingFuture;			//!< future for thread starting pumping
		std::future<void>								m_processingFuture;			//!< future for thread processing queue of incoming trades
	};

}

#endif // _H_3FCFB8B7_AFF8_4292_BDAE_91407D48FE54_
